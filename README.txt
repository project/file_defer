-------------------------------------------------------------------------------
File Defer for Drupal 5.x
  by Ronan Dowling, Gorton Studios - ronan (at) gortonstudios (dot) com	
-------------------------------------------------------------------------------
 
 File Defer is a module which searches for missing files at a remote location.
 ie: If you try to access http://www.example.com/sites/default/files/foo.jpg
     but that file does not exist, this module will fetch the file from another
     server (say http://www.example2.com/sites/default/files/foo.jpg) save it to 
     the local files directory and server the file to the user.
     
Um... Why?
The primary use is for stage or development versions of a website. If you stage
a site at http://stage.example.com and the live version is at 
http://www.example.com, you can set up the stage site to look for missing files
on the live sever negating the need to hand transfer those files while testing.

Another use might be for a poor-man's CDN, but I haven't tried that.

-------------------------------------------------------------------------------
Installation
------------
Go to administer -> site building -> modules and enable the module.

Then go to administer -> settings -> file defer and enter a list of urls to look
for files. Put 1 url on each line, the module will check each url until it finds
the file or runs out of url.

Put the full url of the files directory on the remote site, so if your files 
are stored in files/go/here on the remote site, use 
http://www.example.com/files/go/here/. If the local file being looked for is 
images/thumbnails/1.jpg then the module will search for the file at
http://www.example.com/files/go/here/images/thumbnails/1.jpg.

-------------------------------------------------------------------------------
Known Issues
------------
The module could cause an infinite loop on your server (with Drupal instances 
calling Drupal instances ad infinitum) if you point a drupal installation at 
itself. The module checks for this by not responding if the user-agent string
indicates that the request is coming from a drupal installation. This means that
you cannot string instances of file defer to create a chain of file downloads, 
and the check could be violated if for some reason the user-agent string gets
stripped or modified in the request. BE CAREFUL!!!!

-------------------------------------------------------------------------------
One More Tip
------------
It can be useful to define your defer urls in your settings.php file instead of 
in the database. This allows you to pull a live DB down to your stage site without
overwriting that setting. It can also help prevent inadvertently causing the
issue described above.

-------------------------------------------------------------------------------
TODO
----
* Allow for chaining (one server calling another which calls another etc.)
* More robust infinite loop prevention.
-------------------
